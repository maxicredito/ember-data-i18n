export default {
  model:{
    partner: {
      one: 'Partner',
      other: 'Partners',
      attributes:{
        id: 'Id',
        firstName: 'Name'
      }
    }
  }
};
