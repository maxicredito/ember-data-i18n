export default {
  model:{
    partner: {
      one: 'Associado',
      other: 'Associados',
      attributes:{
        id: 'Id',
        firstName: 'Nome'
      }
    }
  }
};
