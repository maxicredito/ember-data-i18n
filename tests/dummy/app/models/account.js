import DS from 'ember-data';

var Account = DS.Model.extend({
  number: DS.attr('number'),
  partner: DS.belongsTo('partner')
});

Account.reopenClass({
  FIXTURES: [
    { id: 1, number: 12, partner: 1 },
    { id: 2, number: 13, partner: 2}
  ]
});

export default Account;
