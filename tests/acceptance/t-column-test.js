import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../helpers/start-app';

var App;

module("Acceptance: t-column", {
  setup: function(){
    App = startApp();
  },
  teardown: function(){
    Ember.run(App, 'destroy');
  }
});

test('Translate model names', function() {
  expect(1);
  visit('/');
  andThen(function() {
    var text = find('div:contains(Associado)');
    equal(1, text.length);
  });
});
