'use strict';

module.exports = function(/* environment, appConfig */) {
  return {
    APP: {
      defaultLocale: 'pt-br'
    }
  };
};
