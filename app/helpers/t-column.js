import Helper from 'ember-i18n/helper';

export default Ember.Helper.extend({
  i18n: Ember.inject.service(),

  compute(params, interpolations) {
    let p = `model.${params[0]}.attributes.${params[1]}`;
    return this.get('i18n').t(p, interpolations);
  },
});