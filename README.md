# Ember-data-i18n

This README outlines the details of collaborating on this Ember addon.

## Installation

* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).

##To Use
Add in your package.json:

```javascript
"ember-data-i18n":"git+ssh://git@bitbucket.org/maxicredito/ember-data-i18n#master"
```
And ember-cli-i18n too:

```javascript
"ember-cli-i18n": "0.0.4"
```

**Needs the user ssh public key is allowed in bitbucket to download de dependency.**

```hbs
{{!to internacionalize model names}}
{{t-model 'partner'}}
{{!to internacionalize column names}}
{{t-column 'partner' 'firstName'}}
```
And locale file:
```javascript
#app/locales/pt-br.js
export default {
  model:{
    partner: {
      one: 'Associado',
      other: 'Associados',
      attributes:{
        id: 'Id',
        firstName: 'Nome'
      }
    }
  }
};
```


For more information https://github.com/dockyard/ember-cli-i18n.